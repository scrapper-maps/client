import React, { forwardRef } from 'react'
import MaterialTable from 'material-table'
import { 
    AddBox,
    Check,
    ChevronRight,
    ChevronLeft,
    Clear,
    DeleteOutline,
    EditOutlined,
    SaveAlt,
    FilterList,
    FirstPage,
    LastPage,
    Search,
    ArrowDownward,
    Remove,
    ViewColumn,
} from '@material-ui/icons'
import { IconButton } from '@material-ui/core'
function BuildIcons() {
    return {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <EditOutlined {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    }
}

export default function MaterialTableModified (props) {
    const MaterialTableIcons = BuildIcons()
    return (
        <MaterialTable
            {...props}
            icons={MaterialTableIcons}
            components={{
                ...props.components,
                Action: props => {
                    let element = props.action
                    if(typeof props.action === 'function') {
                        element = props.action(props.data)
                        if(element.tooltip === 'Edit'){
                            return(
                                <IconButton 
                                    style={{margin: '0px 5px'}}
                                    variant="outlined" color="primary" size="small"
                                    onClick={(event) => element.onClick(event, props.data)}>
                                    <EditOutlined fontSize="small" />
                                </IconButton>
                            )
                        } else if(element.tooltip === 'Delete'){
                            return(
                                <IconButton 
                                    style={{margin: '0px 5px'}}
                                    variant="outlined" color="secondary" size="small"
                                    onClick={(event) => element.onClick(event, props.data)}>
                                    <DeleteOutline fontSize="small" />
                                </IconButton>
                            )
                        }
                    }
                    return (
                        <IconButton 
                            style={{margin: '0px 5px'}}
                            variant="outlined" size="small" color="primary"
                            onClick={(event) => element.onClick(event, props.data)}>
                            <element.icon />
                        </IconButton>
                    )
                }
            }}
        />
    )
}