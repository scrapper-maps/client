import React from 'react'

import { Box, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { gql, useQuery } from '@apollo/client'

import { ArrowBack } from '@material-ui/icons'
import Header from '../helper/header'
import MaterialTable from '../helper/material-table'

const useStyles = makeStyles({
    content: {
        margin: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
})

function DataInstances ({ history, match }) {
    const classes = useStyles()
    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Name', field: 'instance_name' },
        { title: 'Address', field: 'instance_address' },
        { title: 'Phone', field: 'instance_phone' },
        { title: 'Fetched Time', field: 'create_at' },
    ])

    const findInstancesQuery = gql`query findInstances ($keyword_id: String!){ instances { find(keyword_id: $keyword_id) } }`
    const { loading, data } = useQuery(findInstancesQuery, { variables: { keyword_id: match.params.id }, fetchPolicy: 'no-cache' })
    const instancesData = !loading ? data.instances.find.instances.map(o => ({ ...o })) : []
    const keywordData = !loading ? { ...data.instances.find } : {}
    
    return (
        <Box component="div">
            <Header title="Data Keywords Detail" subtitle="Data Scrapes Result of Selected Keyword">
                <Button type="button" variant="outlined" color="primary"
                    onClick={history.goBack}>
                    <ArrowBack />
                </Button>
            </Header>
            <Box component="div" className={classes.content}>
                <p><b>Keyword:</b> {!loading && keywordData.keyword_name}</p>
                <MaterialTable
                    title=""
                    columns={columns}
                    data={instancesData}
                    options={{exportButton: true}}
                />
            </Box>
        </Box>
    )
}

export default DataInstances