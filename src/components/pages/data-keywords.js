import React from 'react'
import { connect } from "react-redux"
import Header from '../helper/header'
import { Box, Chip } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Button, TextField, Fab, LinearProgress, CircularProgress } from '@material-ui/core'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import { VisibilityOutlined, DoneOutlined, CachedOutlined, CloudUploadOutlined } from '@material-ui/icons'
import MaterialTable from '../helper/material-table'
import { gql, useQuery, useMutation } from '@apollo/client'
import { useSnackbar } from 'notistack'
import { Add } from '@material-ui/icons'
import { useFormik } from 'formik'
import * as Yup from 'yup'

const useStyles = makeStyles({
    content: {
        margin: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    },
    btnUpload: {
        marginRight: '15px'
    }
})

function DataKeywords ({ history }) {
    const classes = useStyles()
    const [openAddDialog, setOpenAddDialog] = React.useState(false)
    const {enqueueSnackbar} = useSnackbar()
    const importKeyword = React.createRef()

    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Keyword', field: 'keyword_name' },
        { title: 'Created Time', field: 'create_at' },
        { title: 'Status', align: 'center', width: 300, render: (row) => {
                if(row.fetched === 0) {
                    return <Chip
                        icon={<CachedOutlined />}
                        label="Currently making magic"
                        variant="outlined"
                    />
                } else if (row.fetched === 1) {
                    return <Chip
                        icon={<DoneOutlined />}
                        label="Fetched Successfully"
                        color="primary"
                        variant="outlined"
                        size="small"
                    />
                }
            }
        },
    ])

    const getKeywordQuery = gql`{ keyword { get } }`
    const { loading, data, refetch } = useQuery(getKeywordQuery)
    const editableData = !loading ? data.keyword.get.map(o => ({ ...o })) : []

    const PlainAddKeywordForm = ({ loader, show_loader, hide_loader }) => {
        const addKeywordMutation = gql`
            mutation addCategory($keyword_name: String!) {
                keyword {
                    create(keyword_name: $keyword_name)
                }
            }
        `
        const [AddKeywordRequest] = useMutation(addKeywordMutation)

        const formik = useFormik({
            initialValues: {
                keyword_name: '',
            },
            validationSchema: Yup.object({
                keyword_name: Yup.string().required('Keyword is required.'),
            }),
            onSubmit: values => {
                show_loader()
                AddKeywordRequest({variables: values}).then((response) => {
                    hide_loader()
                    setOpenAddDialog(false)
                    enqueueSnackbar('Keyword is successfully added.', {variant: 'success'})
                    refetch()
                }).catch((e) => {
                    hide_loader()
                    setOpenAddDialog(false)
                    console.log(e)
                })
            }
        })

        return (
            <Dialog open={openAddDialog} onClose={() => setOpenAddDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    {loader && <LinearProgress />}
                    <DialogTitle id="form-dialog-title">
                        Add Keyword Data
                        <DialogContentText>
                            To add keyword data, please fill all of form input here.
                        </DialogContentText>
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            helperText={formik.errors.keyword_name}
                            margin="dense"
                            onChange={formik.handleChange}
                            id="keyword_name" label="Keyword Name" type="text" fullWidth
                            value={formik.values.keyword_name}/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenAddDialog(false)} variant="contained" color="default" disabled={loader && true}>
                            Cancel
                        </Button>
                        <Button type="submit" variant="contained" color="primary" disabled={loader && true}>
                            {loader && <CircularProgress size={15} />} &nbsp;
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    const mapStateToProps = state => ({
        loader: state.loader
    })
    
    const mapDispatchToProps = dispatch => ({
        show_loader: () => dispatch({ type: 'SHOW_LOADER' }),
        hide_loader: () => dispatch({ type: 'HIDE_LOADER' })
    })
    
    const AddKeywordForm = connect(mapStateToProps, mapDispatchToProps)(PlainAddKeywordForm)

    const DeleteKeywordMutation = gql`
        mutation deleteKeyword($id: String!) {
            keyword {
                delete(id: $id)
            }
        }
    `
    const [DeleteKeywordRequest] = useMutation(DeleteKeywordMutation)
    
    const importMutation = gql`
        mutation saveBundleKeywords ($importFile: [Upload!]!){ 
            keyword { saveImport(file: $importFile) } 
        }
    `
    const [importRequest] = useMutation(importMutation)
    const importKeywordRequest = (e) => {
        if(e.currentTarget.files[0]) {
            importRequest({variables: { importFile: e.currentTarget.files }}).then((response) => {
                enqueueSnackbar('Import is successfully saved.', {variant: 'success'})
                refetch()
            }).catch((e) => {
                enqueueSnackbar(e.message, {variant: 'error'})
                console.log(e)
            })
        }
    }

    return (
        <Box component="div">
            <Header title="Data Keywords" subtitle="Keywords for Data Scrapes">
                <Box component="div" textAlign="right">
                    <Fab color="secondary" size="small" onClick={() => importKeyword.current.click()} className={classes.btnUpload}>
                        <CloudUploadOutlined /> 
                    </Fab>
                    <Fab color="primary" size="small" onClick={() => setOpenAddDialog(true)}>
                        <Add /> 
                    </Fab>
                    <input
                        id="profile-picture" 
                        ref={importKeyword} accept=".xlsx" type="file" hidden
                        onChange={importKeywordRequest}/>
                </Box>
            </Header>
            <Box component="div" className={classes.content}>
                <MaterialTable
                    title=""
                    columns={columns}
                    data={editableData}
                    options={{actionsColumnIndex: 4, exportButton: true}}
                    actions= {[
                        {
                            icon: VisibilityOutlined,
                            tooltip: 'Detail',
                            onClick: (event, rowData) => {
                                history.push(`/admin/keywords/${rowData.id}`)
                            }
                        },
                    ]}
                    editable={{
                        onRowDelete: (oldData) => {
                            return DeleteKeywordRequest({variables: {id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Keyword is successfully deleted.', {variant: 'success'})
                                refetch()
                            }).catch((e) => {
                                enqueueSnackbar(e.message, {variant: 'warning'})
                                console.log(e)
                            })
                        }
                    }}
                />
            </Box>
            <AddKeywordForm />
        </Box>
    )
}

export default DataKeywords