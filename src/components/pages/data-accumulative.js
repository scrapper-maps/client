import React from 'react'
import Header from '../helper/header'
import { connect } from "react-redux"
import { Box, InputLabel, Select, FormControl, MenuItem, TextField, Button, Hidden } from '@material-ui/core'
import { GetAppOutlined } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import { gql, useQuery } from '@apollo/client'
import qs from 'querystring'

// import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
// import DateFnsUtils from '@date-io/date-fns'

import MaterialTableModified from '../helper/material-table'

const useStyles = makeStyles({
    content: {
        margin: '10px',
        '& .MuiPaper-root': {
            padding: '0px 15px'
        },
        '& .MTableToolbar-root-38': {
            display: 'none'
        }
    },
    formControl: {
        margin: '10px',
        width: 'auto',
        '& .search-field': {
            margin:  '0px',
            '& .MuiInputBase-input': {
                padding: '7px 0px'
            }
        }
    }
})

function Dashboard ({ accumulative, set_accumulative, app, session }) {
    const classes = useStyles()
    // const dateFns = new DateFnsUtils()
    
    const [columns] = React.useState([
        { title: '#', field: 'no' },
        { title: 'Name', field: 'instance_name' },
        { title: 'Address', field: 'instance_address' },
        { title: 'Phone', field: 'instance_phone', width: 200 },
        { title: 'Fetched Time', field: 'create_at', width: 200 },
    ])

    const getInstancesQuery = gql`
        query getInstances ($order_by: String!, $order_direction: String!, $page: Int!, $page_size: Int!, $search: String!){ 
            instances { 
                get (
                    order_by: $order_by, 
                    order_direction: $order_direction, 
                    page: $page, 
                    page_size: $page_size, 
                    search: $search
                )
            } 
        }
    `
    const { loading, data } = useQuery(getInstancesQuery, { 
        variables: {
            order_by: accumulative.orderBy,
            order_direction: accumulative.orderDirection,
            page: accumulative.page,
            page_size: accumulative.pageSize,
            search: accumulative.search,
        },
        fetchPolicy: 'no-cache'
    })

    const pageData = !loading ? { ...data.instances.get } : {}
    const pageDataInstances = !loading ? data.instances.get.data.map((val, i) => ({ ...val, no: (accumulative.page * accumulative.pageSize) + i + 1 })) : []

    const queryExport = qs.stringify({
        token: session.token,
        search: accumulative.search,
        order_by: accumulative.orderBy,
        order_direction: accumulative.orderDirection 
    })

    return (
        <Box component="div">
            <Header title="Data Accumulative" subtitle="Data Accumulative of All Keywords">
                <FormControl className={classes.formControl}>
                    <Hidden smDown>
                        <Button type="button" variant="outlined" color="primary" href={`${app.baseurl}/file/export-accumulative?${queryExport}`} 
                            startIcon={<GetAppOutlined />}>
                            Export
                        </Button>
                    </Hidden>
                    <Hidden mdUp>
                        <Button size="small" type="button" variant="outlined" color="primary" href={`${app.baseurl}/file/export-accumulative?${queryExport}`} >
                            <GetAppOutlined />
                        </Button>
                    </Hidden>
                </FormControl>
            </Header>
            <Box component="div" className={classes.content}>
                {/* <MuiPickersUtilsProvider utils={DateFnsUtils}> */}
                    <FormControl className={classes.formControl}>
                        <InputLabel id="order_by">Sort by</InputLabel>
                        <Select
                            labelId="order_by"
                            id="select_order_by"
                            value={accumulative.orderBy}
                            onChange={(__, { props }) => {
                                let changedAccumulative = { ...accumulative }
                                changedAccumulative.orderBy = props.value
                                changedAccumulative.orderDirection = props.value === 'newest_first' ? 'desc' : 'asc'
                                set_accumulative(changedAccumulative)
                            }}
                        >
                            <MenuItem value="newest_first">Newest First</MenuItem>
                            <MenuItem value="instance_name">Instance Name</MenuItem>
                        </Select>
                    </FormControl>
                    {/* <FormControl className={classes.formControl}>
                        <KeyboardDatePicker
                            id="survey-date"
                            format="yyyy-MM-dd"
                            margin="none"
                            label="Form"
                            maxDate={new Date()}
                            // value={formik.values.survey_date}
                            // onChange={(e, value) => formik.setFieldValue('survey_date', value)}
                            KeyboardButtonProps={{
                                'aria-label': 'Filter Date From',
                            }}
                        />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <KeyboardDatePicker
                            id="survey-dates"
                            format="yyyy-MM-dd"
                            margin="none"
                            label="To"
                            maxDate={new Date()}
                            // value={formik.values.survey_date}
                            // onChange={(e, value) => formik.setFieldValue('survey_date', value)}
                            KeyboardButtonProps={{
                                'aria-label': 'Filter Date To',
                            }}
                        />
                    </FormControl> */}
                    <FormControl className={classes.formControl}>
                        <TextField
                            id="search-field" 
                            label="Search" type="text"
                            className="search-field"
                            onKeyPress={({ target, key }) => {
                                if(key === 'Enter') {
                                    let searchValue = target.value.length > 2 ? target.value : ''
                                    set_accumulative({ ...accumulative, search: searchValue, page: 0 })
                                }
                            }}
                            InputLabelProps={{ shrink: true }}
                            placeholder="Type 3 characters first"
                            margin="dense"
                            />
                    </FormControl>
                {/* </MuiPickersUtilsProvider> */}
                {!loading && <MaterialTableModified
                    title="Accumulative Instances Data"
                    columns={columns}
                    onChangePage={ page => {
                        let changedAccumulative = { ...accumulative }
                        changedAccumulative.page = page
                        set_accumulative(changedAccumulative)
                    } }
                    onChangeRowsPerPage={ async pageSize => {
                        let changedAccumulative = { ...accumulative }
                        changedAccumulative.page = 0
                        changedAccumulative.pageSize = pageSize
                        set_accumulative(changedAccumulative)
                    } }
                    options={{ pageSize: accumulative.pageSize, search: false, sorting: false, draggable: false }}
                    data={
                        async __ => ({
                            data: pageDataInstances,
                            page: pageData.page,
                            totalCount: pageData.totalCount,
                        })
                    }
                /> }
            </Box>
        </Box>
    )
}

const mapStateToProps = state => ({
    accumulative: state.accumulative,
    app: state.app,
    session: state.session
})

const mapDispatchToProps = dispatch => ({
    set_accumulative: (payload) => dispatch({ type: 'SET_ACCUMULATIVE_TABLE', payload: payload })
})

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)