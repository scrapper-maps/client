import React from 'react'
import { connect } from 'react-redux'
import Header from '../helper/header'
import defaultDP from '../../images/avatar_male.png'
import { Box, Grid, makeStyles, TextField, Typography, Button, IconButton } from '@material-ui/core'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from '@material-ui/core'
import { EditOutlined } from '@material-ui/icons'
import { Autocomplete } from '@material-ui/lab'
import { useParams } from 'react-router-dom'
import { gql, useQuery, useMutation } from '@apollo/client'
import { useSnackbar } from 'notistack'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import AvatarEditor from 'react-avatar-editor'

import Slider from '@material-ui/core/Slider'

const useStyles = makeStyles(theme => ({
    content: {
        padding: '15px 5px',
    },
    form: {
        margin: '0px 0px 20px 0px',
        padding: '8px 10px',
        boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
        '& .title': {
            fontWeight: 'bold',
            fontSize: '1rem'
        },
        '& .action-container': {
            marginTop: '10px',
            width: '100%',
            textAlign: 'right'
        }
    },
    picture: {
        padding: '20px 10px 0px 10px',
        '& .picture-container': {
            position: 'relative',
            margin: '0px auto',
            maxWidth: '150px',
            padding: '0px 20px',
            '& .profile-picture': {
                width: '150px',
                height: '150px',
                borderRadius: '100px',
            },
            '& .btn-edit-picture': {
                position: "absolute",
                right: '30px',
                background: theme.palette.primary.main,
                color: '#f9f9f9',
                padding: '5px',
                '&:hover': {
                    background: theme.palette.primary.dark
                }
            }
        },
    },
    dialog: {
        '& .avatar-editor-container': {
            padding: '10px 0px',
            borderRadius: '5px',
        },
        '& .avatar-editor-element': {
            display: 'block',
            margin: '0px auto',
        }
    }
}))

function DetailAccount ({ session, app }) {
    const classes = useStyles()
    // console.log(defaultDP)
    const { username } = useParams()
    const { enqueueSnackbar } = useSnackbar()
    const inputProfilePictureRef = React.createRef()

    const getAccountQuery = gql`query getAccount ($id: String!){ account{ view(id: $id), roles } }`
    const {loading, data, refetch} = useQuery(getAccountQuery, { variables: {id: username}, fetchPolicy: 'no-cache' })
    
    const [editAvatarDialog, setEditAvatarDialog] = React.useState(false)
    const [avatar, setAvatar] = React.useState(null)

    const avatarMutation = gql`
        mutation saveAvatar ($id: String!, $picture: [Upload!]!){ 
            account{ savePicture(id: $id, picture: $picture) } 
        }
    `
    const [avatarRequest] = useMutation(avatarMutation)

    const EditAvatarDialog = () => {
        const [scale, setScale] = React.useState(1)
        const [AvatarEditorRef, setAvatarEditorRef] = React.useState(null)
        
        const formik = useFormik({
            initialValues: {},
            onSubmit: _ => {
                const canvas = AvatarEditorRef.getImageScaledToCanvas()
                canvas.toBlob((image) => {
                    const file = new Blob([image], {encoding:null, type: avatar.type})
                    file.name = avatar.name
                    avatarRequest({variables: { id: username, picture: file}}).then((response) => {
                        setAvatar(null)
                        enqueueSnackbar('Profile picture is successfully updated.', {variant: 'success'})
                        setEditAvatarDialog(false)
                        window.location.reload(false)
                    }).catch((e) => {
                        enqueueSnackbar(e.message, {variant: 'error'})
                        setEditAvatarDialog(false)
                        console.log(e)
                    })
                })
            }
        })

        return (
            <Dialog className={classes.dialog} open={editAvatarDialog} onClose={() => setEditAvatarDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle id="form-dialog-title">
                        Edit Profile Picture
                        <DialogContentText>
                            To add profile picture, please fill all of form inputs here.
                        </DialogContentText>
                    </DialogTitle>
                    <DialogContent>
                        <Box className="avatar-editor-container">
                            <AvatarEditor
                                className="avatar-editor-element"
                                ref={(ref) => setAvatarEditorRef(ref)}
                                image={avatar}
                                border={25}
                                width={200}
                                height={200}
                                color={[241, 243, 248, 0.6]} 
                                scale={scale}
                                borderRadius={250}
                            />
                        </Box>
                        <Slider 
                            step={0.01} min={1} max={3}
                            onChange={(e, val) => setScale(val)} 
                            aria-labelledby="scale-slider"
                            value={scale} /> 
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setEditAvatarDialog(false)} variant="contained" size="small" color="default">
                            Cancel
                        </Button>
                        <Button type="submit" variant="contained" size="small" color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }
    
    const accountDataMutation = gql`
        mutation saveAccountData ($username: String!, $name: String!, $address: String!){ 
            account{ savePersonalData(username: $username, name: $name, address: $address) } 
        }
    `
    const [accountDataRequest] = useMutation(accountDataMutation)

    const formAccountData = useFormik({
        initialValues: {
            name: !loading ? data.account.view.name : '',
            address: !loading ? data.account.view.address : '',
        },
        enableReinitialize: true,
        validationSchema: Yup.object({
            name: Yup.string().required(),
            address: Yup.string().required(),
        }),
        onSubmit: values => {
            accountDataRequest({variables: {...values, username: username}}).then((response) => {
                enqueueSnackbar('Account Data is successfully updated.', {variant: 'success'})
                refetch()
            }).catch((e) => {
                enqueueSnackbar(e.message, {variant: 'error'})
                console.log(e)
            })
        }
    })

    const rolesDataMutation = gql`
        mutation saveRolesData ($username: String!, $roles: [String]!){ 
            account{ saveRoles(username: $username, roles: $roles) } 
        }
    `
    const [rolesDataRequest] = useMutation(rolesDataMutation)

    const formRoles = useFormik({
        initialValues: {
            roles: !loading ? data.account.view.roles : [],
        },
        enableReinitialize: true,
        validationSchema: Yup.object({
            roles: Yup.array().required('Roles is required.'),
        }),
        onSubmit: values => {
            values.roles = values.roles.map(val => val.rolename)

            rolesDataRequest({variables: {...values, username: username}}).then((response) => {
                enqueueSnackbar('Roles Data is successfully updated.', {variant: 'success'})
                refetch()
            }).catch((e) => {
                enqueueSnackbar(e.message, {variant: 'error'})
                console.log(e)
            })
        }
    })

    const passwordChangeMutation = gql`
        mutation savePasswordChange ($username: String!, $password: String!, $repassword: String!){ 
            account{ savePassword(username: $username, password: $password, repassword: $repassword) } 
        }
    `
    const [passwordChangeRequest] = useMutation(passwordChangeMutation)

    const formPassword = useFormik({
        initialValues: {
            password: '',
            repassword: '',
        },
        enableReinitialize: true,
        validationSchema: Yup.object({
            password: Yup.string().required('Password is required.'),
            repassword: Yup.string().required('Repassword is required.').oneOf([Yup.ref('password')], 'Password does not match'),
        }),
        onSubmit: (values, { resetForm }) => {
            passwordChangeRequest({variables: {...values, username: username}}).then((response) => {
                enqueueSnackbar('Password is successfully changed.', {variant: 'success'})
                resetForm()
                refetch()
            }).catch((e) => {
                enqueueSnackbar(e.message, {variant: 'error'})
                console.log(e)
            })
        }
    })

    return (
        <Box component="div">
            <Header 
                title="Account Management"
                breadcrumb={[
                    {display: 'Super Admin', active: false, url: '/admin/account'},
                    {display: 'Account Management', active: false, url: '/admin/account'},
                    {display: username, active: true},
                ]} />
            <Box className={classes.content}>
                <Box className={classes.form}>
                    <Grid container spacing={3}>
                        <Grid item md={4} sm={12} xs={12}>
                            <Typography className="title">Edit Profile Picture</Typography>
                            <Typography color="textSecondary">Upload account's profile picture here</Typography>
                            <Box className={classes.picture}>
                                <Box className="picture-container">
                                    <img 
                                        className='profile-picture' 
                                        src={!loading ? data.account.view.photo ? `${app.baseurl}/file/display-picture/${data.account.view.username}?token=${session.token}` : defaultDP : defaultDP} 
                                        alt="Default Profile" />
                                    <IconButton
                                        className="btn-edit-picture"
                                        aria-label="Edit Profile Picture"
                                        onClick={() => inputProfilePictureRef.current.click()}
                                        color="primary">
                                        <EditOutlined />
                                    </IconButton>
                                    <input
                                        id="profile-picture" 
                                        ref={inputProfilePictureRef} accept="image/*" type="file" hidden
                                        onChange={(e) => {
                                            if(e.currentTarget.files[0]) {
                                                setAvatar(e.currentTarget.files[0])
                                                setEditAvatarDialog(true)
                                            }
                                        }}/>
                                </Box>
                            </Box>
                        </Grid>
                        <Grid item md={8} sm={12} xs={12}>
                            <Box className={classes.form}>
                                <Typography className="title">Edit Account Data</Typography>
                                <Typography color="textSecondary">Update account's personal data here</Typography>
                                <form onSubmit={formAccountData.handleSubmit}>
                                    <TextField
                                        helperText={formAccountData.errors.name}
                                        margin="dense"
                                        onChange={formAccountData.handleChange} 
                                        id="name" label="Full Name" type="text" fullWidth
                                        value={formAccountData.values.name}/>
                                    <TextField
                                        helperText={formAccountData.errors.address}
                                        margin="dense"
                                        onChange={formAccountData.handleChange}
                                        id="address" label="Address" type="text" fullWidth
                                        value={formAccountData.values.address}/>
                                    <Box className="action-container">
                                        <Button type="submit" color="primary" size="small" variant="contained">
                                            Save Account Data
                                        </Button>
                                    </Box>
                                </form>
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
                <Box className={classes.form}>
                    <Typography className="title">Edit Roles</Typography>
                    <Typography color="textSecondary">Change account's roles here</Typography>
                    <form onSubmit={formRoles.handleSubmit}>
                        <Autocomplete
                            multiple
                            id="roles-autocomplete"
                            size="small"
                            onChange={(e, value) => formRoles.setFieldValue('roles', value)}
                            value={formRoles.values.roles}
                            options={!loading ? data.account.roles : []}
                            getOptionLabel={(option) => option.roledesc }
                            getOptionSelected={(option, value) => option.rolename === value.rolename}
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formRoles.errors.roles} 
                                    margin="dense" 
                                />
                            }/>
                        <Box className="action-container">
                            <Button type="submit" color="primary" size="small" variant="contained">
                                Save Roles
                            </Button>
                        </Box>
                    </form>
                </Box>
                <Box className={classes.form}>
                    <Typography className="title">Change Password</Typography>
                    <Typography color="textSecondary">Change account's password here</Typography>
                    <form onSubmit={formPassword.handleSubmit}>
                        <TextField
                            helperText={formPassword.errors.password}
                            margin="dense"
                            onChange={formPassword.handleChange}
                            id="password" label="New Password" type="password" fullWidth
                            value={formPassword.values.password}/>
                        <TextField
                            helperText={formPassword.errors.repassword}
                            margin="dense"
                            onChange={formPassword.handleChange}
                            id="repassword" label="Repeat Password" type="password" fullWidth
                            value={formPassword.values.repassword}/>
                        <Box className="action-container">
                            <Button type="submit" color="primary" size="small" variant="contained">
                                Save Password
                            </Button>
                        </Box>
                    </form>
                </Box>
            </Box>
            {editAvatarDialog ? <EditAvatarDialog /> : null} 
        </Box>
    )
}
const mapStateToProps = state => ({
    app: state.app,
    session: state.session
})

export default connect(mapStateToProps)(DetailAccount)