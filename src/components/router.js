import React from 'react'

import qs from 'querystring'

import { connect } from 'react-redux'
import { BrowserRouter, Route } from 'react-router-dom'

import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client'
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from '@apollo/client/link/context';

import Middleware from './middleware/middleware'
import PrivateRoute from './middleware/private-router'

import Login from './pages/login'
import DataAccumulative from './pages/data-accumulative'
import DataKeywords from './pages/data-keywords'
import DataKeywordsDetail from './pages/data-keywords-detail'
import Account from './pages/account'
import AccountDetail from './pages/account.detail'
import SettingsProfile from './pages/settings.profile'

function Router ({ session, app }) {
    // Setup Apollo Client
    const __baseGraphQLUrl = `${app.baseurl}/graphql`

    const authLink = setContext((_, { headers }) => {
        const authorization = qs.stringify({token: session.token})
        return { headers: { ...headers, authorization } }
    })

    const client = new ApolloClient({ 
        link: authLink.concat(createUploadLink({ uri: __baseGraphQLUrl })), 
        cache: new InMemoryCache(), 
        errorPolicy: 'all' 
    })

    return (
        <ApolloProvider client={client}>
            <Middleware>
                <BrowserRouter>
                    <Route exact path='/' component={ Login } />
                    <Route exact path='/login' component={ Login } />
                    <PrivateRoute path='/admin' component={ DataAccumulative } theme = 'admin'>
                        <Route exact path='/admin/accumulative' component={ DataAccumulative } />
                        <Route exact path='/admin/keywords' component={ DataKeywords } />
                        <Route exact path='/admin/keywords/:id' component={ DataKeywordsDetail } activeMenu="/admin/keywords" />
                        <Route exact path='/admin/account' component={ Account } />
                        <Route exact path='/admin/account/:username' component={ AccountDetail } activeMenu="/admin/account" />
                        <Route exact path='/admin/profile' component={ SettingsProfile } activeMenu="/admin/profile" />
                    </PrivateRoute>
                </BrowserRouter>
            </Middleware>
        </ApolloProvider>
    )
}

const mapStateToProps = state => ({
    session: state.session,
    app: state.app
})

export default connect(mapStateToProps)(Router)