import React from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/styles'
import { Box, Hidden, Drawer, Divider } from '@material-ui/core'

import AdminAppBar from './admin-appbar'
import AdminMenu from './admin-menu'
import AdminDisplay from './admin-display'
import AdminFooter from './admin-footer'

const useStyles = makeStyles(theme => ({
    /** Sidebar */
    sidebar: {
        width: '240px',
        transform: 'none',
    },
    sidebarhide: {
        transition: 'transform 200ms cubic-bezier(2, 0, 0, 0) 200ms',
        transform: 'translateX(-240px)',
    },
}))

function AdminSidebar({ admin_sidebar }) {
    const classes = useStyles()
    return (
        <Box>
            <Hidden xsDown>
                <Drawer
                    classes={{ root: admin_sidebar ? classes.sidebar : classes.sidebarhide }}
                    variant="persistent"
                    open={admin_sidebar} >
                    <AdminDisplay /> 
                    <Divider /> 
                    <AdminMenu />
                </Drawer>
            </Hidden>
            <Hidden smUp>
                <Drawer
                    classes={{ 
                        root: !admin_sidebar ? classes.sidebar : classes.sidebarhide,
                        paper: classes.paper
                    }}
                    variant='temporary'
                    open={!admin_sidebar} >
                        <AdminAppBar />
                        <AdminDisplay /> 
                        <Divider /> 
                        <AdminMenu />
                        <AdminFooter />
                </Drawer>
            </Hidden>
        </Box>
    )
}

const mapStateToProps = state => ({
    admin_sidebar: state.admin_sidebar,
})

export default connect(mapStateToProps, null)(AdminSidebar)