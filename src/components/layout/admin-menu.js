import React from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/styles'
import { withRouter } from 'react-router-dom'
import { List, Typography, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import * as Icons from './admin-icons'

const useStyles = makeStyles(theme => ({
    /** Sidebar */
    sidebar: {
        width: '240px',
        transform: 'none',
    },
    sidebarhide: {
        transition: 'transform 200ms cubic-bezier(2, 0, 0, 0) 200ms',
        transform: 'translateX(-240px)',
    },
    labelMenu: {
        fontSize: '12px',
        padding: '3px 10px',
        color: 'darkgray',
    },
    list: {
        paddingLeft: '4px'
    },
    listItem: {
        padding: '4px 16px',
        '&.active': {
            borderLeft: '3px solid blue',
            background: 'lightblue'
        }
    },
    listItemIcon: {
        minWidth: '40px',
    },
    listItemText: {
        fontSize: '0.8rem',
        paddingTop: '4px',
        color: '#000'
    },
}))

function AdminMenu({ history, session, active_menu, changeActiveMenu }) {
    const classes = useStyles()
    return (
        <List className={classes.list}>
            {session.userinfo.menu.map((list) => {
                return (
                    <span key={list.label}>
                        <Typography className={classes.labelMenu}>{list.label}</Typography>
                        {list.menu.map((item) => {
                            const ListIcon = Icons[item.icons]
                            return (
                                <ListItem 
                                    className={item.url === active_menu ? 'active':''} 
                                    classes={{root: classes.listItem}} 
                                    key={item.url}
                                    onClick={() => {
                                        changeActiveMenu(item.url)
                                        history.push(item.url)
                                    }}
                                    button>
                                    <ListItemIcon classes={{root: classes.listItemIcon}}><ListIcon /></ListItemIcon>
                                    <ListItemText classes={{root: classes.listItemText}} primary={item.title} />
                                </ListItem>
                            )
                        })}
                    </span>
                )
            })}
        </List>
    )
}

const mapStateToProps = state => ({
    session: state.session,
    active_menu: state.active_menu,
})

const mapDispatchToProps = dispatch => ({
    changeActiveMenu: (payload) => dispatch({type: 'CHANGE_ACTIVE_MENU', payload}),
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AdminMenu))