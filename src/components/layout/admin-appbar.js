import React from 'react'
import { connect } from 'react-redux'
import { makeStyles } from "@material-ui/styles";
import { AppBar, Toolbar, Box, Typography, Button, Menu, MenuItem } from "@material-ui/core";
import { Settings, Menu as MenuIcon } from './admin-icons'

import Logo from '../../images/logo.png'

const useStyles = makeStyles(theme => ({
    header: {
        zIndex: '1250',
    },
    toolbar: {
        minHeight: '44px'
    },
    brand: {
        display: 'flex',
        flex: 1,
    },
    brandText: {
        paddingTop: '5px',
        fontSize: '1.2rem',
        width: '210px',
    },
    brandLogo: {
        padding: '0px 10px 0px 0px',
        width: '16px',
        height: '16px',
    },
    menuIcon: {
        color: 'white',
    },
    rightIcon: {
        color: 'white',
    },
}))

function AdminAppBar ({ toggleSidebar, toggleAnchor, app, admin_anchor, logout }) {
    const classes = useStyles()
    return (
        <AppBar classes={{root: classes.header}}>
            <Toolbar classes={{root: classes.toolbar}}>
                <Box className={classes.brand}>
                    <Typography className={classes.brandText}>
                        <img className={classes.brandLogo} src={Logo} alt="Go to Dashboard" />
                        {app.brand}
                    </Typography>
                    <Button onClick={() => { toggleSidebar() }}>
                        <MenuIcon className={classes.menuIcon} />
                    </Button>
                </Box>
                <Button aria-controls="simple-menu" aria-haspopup="true" onClick={(e) => toggleAnchor(e.currentTarget)}>
                    <Settings className={classes.rightIcon} />
                </Button>
                <Menu 
                    id="simple-menu"
                    keepMounted
                    anchorEl={admin_anchor}
                    open={Boolean(admin_anchor)}
                    onClose={() => toggleAnchor(null)}
                    classes={{paper: classes.rightMenu}}>
                    <MenuItem component="a" href="/admin/profile">My Account</MenuItem>
                    <MenuItem onClick={() => { toggleAnchor(null); logout() }}>Logout</MenuItem>
                </Menu>
            </Toolbar>
        </AppBar>
    )
}

const mapStateToProps = state => ({
    app: state.app,
    admin_anchor: state.admin_anchor
})

const mapDispatchToProps = dispatch => ({
    toggleSidebar: () => dispatch({type: 'TOGGLE_SIDEBAR'}),
    toggleAnchor: (payload) => dispatch({type: 'TOGGLE_ANCHOR', payload}),
    logout: () => dispatch({type: 'LOGOUT'}),
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminAppBar)