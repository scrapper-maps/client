import React from 'react'
import { connect } from "react-redux"
import { Grid, Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'

import AdminSidebar from './admin-sidebar'
import AdminAppBar from './admin-appbar'
import AdminFooter from './admin-footer'

const useStyles = makeStyles(theme => ({
    /** Content */
    main: {
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        marginTop: '35px',
        '& .MuiDrawer-paper': {
            width: '240px'
        }
    },
    content: {
        width: '100%',
        padding: '5px',
        marginBottom: '40px'
    },
}))

function AdminLayout ({ children, changeActiveMenu }) {
    const classes = useStyles()

    return (
        <Grid container direction="row">
            <AdminAppBar />
            <Box className={classes.main}>
                <AdminSidebar />
                <div className={classes.content}>
                    {children.map((child, key) => {
                        const RenderComponent = withRouter(child.props.component)
                        const ChildComponent = React.cloneElement(child, { 
                            ...child.props,
                            key: key,
                            component: null,
                            render: () => {
                                const activeMenuTemp = child.props.activeMenu ? child.props.activeMenu : child.props.path
                                changeActiveMenu(activeMenuTemp)
                                return <RenderComponent />
                            }
                        })
                        
                        return ChildComponent
                    })}
                </div>
            </Box>
            <AdminFooter />
        </Grid>
    )
}

const mapDispatchToProps = dispatch => ({
    changeActiveMenu: (payload) => dispatch({type: 'CHANGE_ACTIVE_MENU', payload}),
})

export default connect(null, mapDispatchToProps)(AdminLayout)