import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Typography } from "@material-ui/core"

const useStyles = makeStyles(theme => ({
    footer: {
        bottom: '0',
        left: 'auto',
        right: '0',
        position: 'fixed',
        zIndex: '1250',
        backgroundColor: '#EEEEEE',
        width: '100%',
        height: '35px',
        paddingTop: '10px',
        '& .footer-text': {
            padding: '0px 20px',
            fontWeight: 'bold'
        }
    }
}))

export default function Footer () {
    const classes = useStyles()
    return (
        <Box className={classes.footer}>
            <Typography className="footer-text">
                Copyright &copy; 2020 Annafia. All right reserved.
            </Typography>
        </Box>
    )
}