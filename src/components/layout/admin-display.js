import React from 'react'
import { connect } from "react-redux"
import { makeStyles } from '@material-ui/core/styles'
import { Box, Typography } from "@material-ui/core"
import defaultDP from '../../images/avatar_male.png'

const useStyles = makeStyles(theme => ({
    display: {
        textAlign: 'center',
        marginTop: '30px',
        padding: '20px 10px 10px 10px',
    },
    displayRole: {
        fontSize: '0.8rem',
        fontWeight: 'bold'
    },
    displayPicture: {
        width: '90px',
        height: '90px',
        padding: '10px',
        borderRadius: '100px'
    },
}))

function AdminDisplay ({ session, app }) {
    const classes = useStyles()
    return (
        <Box className={classes.display}>
            <img 
                className={classes.displayPicture} 
                src={session.userinfo.hasPhoto ? `${app.baseurl}/file/display-picture?token=${session.token}`: defaultDP} 
                alt="Default Profile" />
            <Typography>{session.userinfo.name}</Typography>
            <Typography className={classes.displayRole}>Administrator</Typography>
        </Box>
    )
}

const mapStateToProps = state => ({
    app: state.app,
    session: state.session,
})

export default connect(mapStateToProps)(AdminDisplay)