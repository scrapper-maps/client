import React from 'react'
import { connect } from "react-redux";
import { Route, withRouter, Redirect } from 'react-router-dom'
import Admin from '../layout/admin-layout'

function Routes (props) {
    const Component = withRouter(props.component)
    const Theme = props.theme === 'admin' ? withRouter(Admin) : null
    if (props.session.authorize === -1) {
        return null
    } else if(props.session.authenticate && props.session.authorize === 1) {
        if(Theme) {
            return (<Route path={props.path} render={() => <Theme {...props} />} />)
        } else {
            return <Route path={props.path} render={() => Component}></Route>
        }
    } else {
        return <Route path={props.path} render={() => <Redirect to='/login' />} />
    }
}

const mapStateToProps = state => ({
    session: state.session
})

export default connect(mapStateToProps)(withRouter(Routes))