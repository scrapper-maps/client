import { connect } from 'react-redux'
import { gql, useQuery } from '@apollo/client'

const Middleware = ({ children, session, authorize, notAuthorize }) => {
    const userinfoQuery = gql`{ userinfo }`
    
    const { loading } = useQuery(userinfoQuery, {
        onCompleted: (response) => {
            if(session.authorize !== 1 && !response.userinfo.unauthorized) {
                authorize(response.userinfo)
            } else if(session.authorize !== 0 && response.userinfo.unauthorized) {
                notAuthorize()
            }
        }
    })

    return !loading && session.authorize !== -1 ? children : null
}

const mapStateToProps = state => ({
    session: state.session
})

const mapDispatchToProps = dispatch => {
    return {
        authorize: (payload) => dispatch({ type: 'AUTHORIZE', payload }),
        notAuthorize: () => dispatch({ type: 'NOT_AUTHORIZE' }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Middleware)