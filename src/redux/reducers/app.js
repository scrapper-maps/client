import env from '../../config/env'

const initialState = {
    appname: env.appname,
    brand: env.brand,
    baseurl: `http${env.ssl?'s':''}://${env.hostname}/${env.api}`
}

export default function AppReducers (state = initialState, action = {}) {
    switch(action.type) {
        default: return state
    }
}