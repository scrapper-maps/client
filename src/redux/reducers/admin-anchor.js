import { TOGGLE_ANCHOR } from '../root-action-types'

export default function AdminAnchorReducers (state = null, action = {}) {
    switch(action.type) {
        case TOGGLE_ANCHOR:
            if(state !== action.payload) {
                return action.payload
            }
            return state
        default: 
            return state
    }
}