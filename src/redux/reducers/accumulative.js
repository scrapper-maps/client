import { SET_ACCUMULATIVE_TABLE } from '../root-action-types'

const initialState = {
    orderBy: "newest_first",
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
}

export default function AccumulativeTableReducers (state = initialState, action = {}) {
    switch(action.type) {
        case SET_ACCUMULATIVE_TABLE: 
            if (
                state.orderBy !== action.payload.orderBy ||
                state.orderDirection !== action.payload.orderDirection ||
                state.page !== action.payload.page ||
                state.pageSize !== action.payload.pageSize ||
                state.search !== action.payload.search
            ) {
                return action.payload
            } else {
                return state
            }
        default: 
            return state
    }
}