import { AUTHENTICATE, NOT_AUTHORIZE, AUTHORIZE, LOGOUT } from '../root-action-types'

const initialState = localStorage.getItem('session') ? JSON.parse(localStorage.getItem('session')) : {}
export default function SessionReducers (state = initialState, action = {}) {
    let nextState = {}
    switch (action.type) {
        case AUTHENTICATE:
            nextState = { authenticate: true, authorize: -1, token: action.payload, userinfo: {} }
            localStorage.setItem('session', JSON.stringify(nextState)) 
            return nextState
        case NOT_AUTHORIZE:
            nextState = {...state, authorize: -1 }
            localStorage.setItem('session', JSON.stringify(nextState)) 
            nextState.authorize = 0
            return nextState
        case AUTHORIZE:
            nextState = {...state, authorize: -1, userinfo: action.payload }
            localStorage.setItem('session', JSON.stringify(nextState)) 
            nextState.authorize = 1
            return nextState
        case LOGOUT:
            nextState = { authenticate: false, authorize: false, token: null, userinfo: {} }
            localStorage.setItem('session', JSON.stringify(nextState)) 
            return nextState
        default: 
            return state
    }
}