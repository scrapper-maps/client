import { CHANGE_ACTIVE_MENU } from '../root-action-types'

export default function ActiveMenuReducers (state = null, action = {}) {
    switch(action.type) {
        case CHANGE_ACTIVE_MENU: 
            if(state !== action.payload) { return action.payload }
            return state
        default: 
            return state
    }
}