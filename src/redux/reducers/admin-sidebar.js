import { TOGGLE_SIDEBAR } from '../root-action-types'

export default function AdminSidebarReducers (state = true, action = {}) {
    switch(action.type) {
        case TOGGLE_SIDEBAR:
            if(state === false) { return true }
            return false
        default: 
            return state
    }
}