import { SHOW_LOADER, HIDE_LOADER } from '../root-action-types'

export default function LoaderReducers (state = false, action = {}) {
    switch(action.type) {
        case SHOW_LOADER: return true
        case HIDE_LOADER: return false
        default: return state
    }
}