import { combineReducers } from 'redux'

import active_menu from './reducers/active-menu'
import admin_sidebar from './reducers/admin-sidebar'
import admin_anchor from './reducers/admin-anchor'
import loader from './reducers/loader'
import session from './reducers/session'
import accumulative from './reducers/accumulative'
import app from './reducers/app'

export default combineReducers({
    app,
    active_menu,
    admin_sidebar,
    admin_anchor,
    loader,
    session,
    accumulative,
})