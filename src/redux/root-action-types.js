const 
    AUTHENTICATE = 'AUTHENTICATE',
    NOT_AUTHORIZE = 'NOT_AUTHORIZE',
    AUTHORIZE = 'AUTHORIZE',
    LOGOUT = 'LOGOUT',
    CHANGE_ACTIVE_MENU = 'CHANGE_ACTIVE_MENU',
    TOGGLE_ANCHOR = 'TOGGLE_ANCHOR',
    TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR',
    SHOW_LOADER = 'SHOW_LOADER',
    HIDE_LOADER = 'HIDE_LOADER',
    SET_ACCUMULATIVE_TABLE = 'SET_ACCUMULATIVE_TABLE'

export {
    AUTHENTICATE,
    NOT_AUTHORIZE,
    AUTHORIZE,
    LOGOUT,
    CHANGE_ACTIVE_MENU,
    TOGGLE_ANCHOR,
    TOGGLE_SIDEBAR,
    SHOW_LOADER,
    HIDE_LOADER,
    SET_ACCUMULATIVE_TABLE,
}