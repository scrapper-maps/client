import React from 'react'

import Router from './components/router'
import rootReducer from './redux/root-reducer'

import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { Provider } from 'react-redux'

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import { SnackbarProvider } from 'notistack'

function App (props) {
    // Setup Redux Store
    const store = createStore(
        rootReducer,
        composeWithDevTools(
            applyMiddleware(thunk)
        )
    )

    const theme = createMuiTheme({
        typography: {
            fontFamily: ['Helvetica','sans-serif'].join(','),
            color: '#fff',
            body1: {
                fontSize: 14
            }
        },
        overrides: {
            MuiCssBaseline: {
                "@global": {
                    body: {
                        margin: '0px'
                    }
                }
            },
            MuiTableCell: {
                root: {
                    padding: '10px'
                },
                head: {
                    fontWeight: 'bold'
                }
            },
            MuiTableHead: {
                root: {
                    borderTop: '2px solid lightgrey'
                }
            },
            MuiToolbar: {
                root: {
                    minHeight: 0
                }
            },
            MuiDialog: {
                paper: {
                    borderRadius: '5px',
                }
            },
            MuiDialogTitle: {
                root: {
                    padding: '16px 24px 0px 24px',
                    borderBottom: '2px solid darkgrey'
                }
            },
            MuiDialogActions: {
                root: {
                    padding: '10px 24px 24px 24px',
                }
            },
            MuiDialogContent: {
                root: {
                    padding: '0px 24px',
                    paddingBottom: '10px'
                }
            }
        }
    })
    
    return ( 
        <Provider store={store}> 
            <ThemeProvider theme={theme}>
                <SnackbarProvider maxSnack={3}>
                    <Router /> 
                </SnackbarProvider>
            </ThemeProvider>
        </Provider> 
    )
}

export default App